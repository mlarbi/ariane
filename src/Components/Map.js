import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import GoogleDocs from './GoogleDocs.js';

const MapComponent = () => {
    const mapContainer = useRef(null);

    useEffect(() => {
        mapboxgl.accessToken = 'pk.eyJ1IjoibGVsaWFzaW1wbG9uIiwiYSI6ImNreWU5OGxwazAwcWEycGxnOTZobTN1aDIifQ.xHqgEeiv-to4-vhnu0o2EQ';

        const map = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/light-v11',
            center: [2.349014, 48.864716],
            zoom: 3,
        });


        /// API keys and the document-id
        /// API keys and the document-id
        /// API keys and the document-id
        /// API keys and the document-id

        var spreadsheetId="1HFATzyomNGgmxW6Zd6CTp87RA0ERLx4QNXeH97-sn4M" 
        var apiKey="AIzaSyAjJ_4wWdxTKsy4iQpoB8tr29qXOjZuZo0"
        const geoapify_api_Key = "839558bc8046469cb32157a355e19303"
        
        // fetch the google document Data
        // fetch the google document Data
        // fetch the google document Data

        GoogleDocs(spreadsheetId,apiKey).then( data =>{

            var my_data =JSON.parse(data)
            var index=1;
            var old_cords = [];
            my_data.forEach( item => {

                // use console.log(item) to see all the info about it 
                // use console.log(item) to see all the info about it 
                // use console.log(item) to see all the info about it 

                var Pays = item[8]
                var Ville = item[7]
                
                var addr= encodeURIComponent(Pays+" "+Ville)
                var url="https://api.geoapify.com/v1/geocode/search?text="+addr+"&apiKey="+geoapify_api_Key
                let coordonnees=[];

                // get the coordinates using the Pays/Ville
                // get the coordinates using the Pays/Ville
                // get the coordinates using the Pays/Ville


                fetch(url).then(response => response.json())
                  .then(result => coordonnees = result.features[0].geometry.coordinates)
                  .then(()=>{
                   
                       // add a gaps between the places if they have the same geolocation
                       // add a gaps between the places if they have the same geolocation
                       // add a gaps between the places if they have the same geolocation


                        if(old_cords.indexOf(JSON.stringify(coordonnees,null,2)) !== -1) {
                            const offset = 0.0002 * (index + 1); // Ajustez le décalage selon vos besoins
                            coordonnees[0] += offset;
                            coordonnees[1] += offset;
                            index = index + 1;
                            old_cords.push(JSON.stringify(coordonnees,null,2))
                        } else {
                            old_cords.push(JSON.stringify(coordonnees,null,2))
                        }
                       
                       // create the popup with a red color 
                       // create the popup with a red color 
                       // create the popup with a red color 
                       // create the popup with a red color 

                        try {
                             var Prenom = item[2]
                             var Nom = item[1]
                             var Lab = item[12]
                             
                             const popupContainer = document.createElement('div');
                             popupContainer.innerHTML = `
                                    <h3>${Prenom}</h3>
                                    <h3>${Nom}</h3>
                                    <p>${Lab == null ? "" : Lab}</p>
                              `;

                             // Créez le popup et associez-le au marqueur
                             const popup = new mapboxgl.Popup({
                                    offset: 50, // Ajustez le décalage du popup par rapport au marqueur
                             }).setDOMContent(popupContainer);

                             const marker = new mapboxgl.Marker({ "color": "#8F0006" })
                                    .setLngLat(coordonnees)
                                    .setPopup(popup)
                                    .addTo(map);

                        }catch(e){
                            return 0;
                        }
                        
                        // end 
                        // end 
                        // end 

               })

            });
        

        });

   

        // Ajouter un contrôleur de zoom
        const navControl = new mapboxgl.NavigationControl();
        map.addControl(navControl);

        return () => map.remove();
    }, []);

    return <div id='membres' className='mt-10 border-l border-r border-arianBord' ref={mapContainer} style={{ width: '100%', height: '400px' }} />;
};

export default MapComponent;
