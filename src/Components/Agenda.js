
import Accueil from "./Accueil";

import Headers from "./Headers";


export default function Agenda() {
    return (
        <article>
             <Headers titre={"L'Agenda"} theme={"L'Agenda d'activités du Consortium Ariane est élaboré par ses membres et groupes de travail. Cet agenda présente toute la variété d'activités du Consortium. "} />
             <div Style='width: 100%; display:flex; justify-content:center;padding:20px;'>
                     <div id='agenda' Style='width: 60%; height:300;  '></div></div>
           <div className="modal fade" id="myModal" data-backdrop="false"  Style='top:10vh' role="dialog">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div className="modal-body">
                    </div>
                    <div className="modal-footer">
                      <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>    
              </div>
            </div>
        </article>
    )
}