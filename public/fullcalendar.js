$(document).ready(function() {
  if((window.location.hash.substring(1)).length ==0){
    $(location).prop('href',location.protocol + '//' + location.host + location.pathname+"#month");
  }
  $('#agenda').fullCalendar({
        locale:'fr',
        defaultView: window.location.hash.substring(1),
        customButtons: {
          years: {
            text: 'Annee',
            click: function() {
              return 0;
            }
          },
          months: {
            text: 'Mois',
            click: function() {
              $(location).prop('href',location.protocol + '//' + location.host + location.pathname+"#month");
              $("body").append('<meta http-equiv="refresh" content="0">')
            }
          },
          viewlist: {
            text: 'Agenda',
            click: function() {
             $(location).prop('href',location.protocol + '//' + location.host + location.pathname+"#listMonth");
              $("body").append('<meta http-equiv="refresh" content="0">')     
            }
          }
        },eventLimitClick:function(){
          
        },
        eventClick: function(calEvent, jsEvent, view){
          if (calEvent.url) {
              var description = calEvent.description;
              var title = calEvent.title;
              var url = calEvent.url;
              ////

              var date_start = calEvent._start._i;console.log(date_start);

              var date_end = calEvent._end._i;console.log(date_end);

              var location = calEvent.location;
              var position = $(this).offset()
              $(this).parent().append('<button type="button" data-toggle="modal" data-target="#myModal" id="descButtt" style="display:none"></button>');
              // title
              $(".modal-header h4").remove();
              $(".modal-header").append("<h4 style='color:#3A87AD'><a href='"+url+"' target='_blank'>"+title+"</a></h4>");
              // where // when // description
              $(".modal-body").empty();
              $(".modal-body").append("<div class='.where'><b>Where:<br></b><p>"+location+"</p></div>");
              $(".modal-body").append("<div class='.when'><b>When:<br></b><p>"+date_start+" - "+date_end+"</p></div>");
              $(".modal-body").append("<div class='.descrip-to'><b>Description:<br></b><p>"+description+"</p></div>");
             
              $("#descButtt").click();
              
              return false;
          }
        },  
        header:{
          left:   'prevYear,years,nextYear',
          center: 'title',
          right:  'viewlist,today prev,months,next'
        },
        buttonText:{
          today:"this day"
        },
        googleCalendarApiKey: 'AIzaSyD9I9bVdtVrLiYpUHMWsbgZTDSVguMelR8',
        events: {
            googleCalendarId: 'consortium.ariane@gmail.com'
        }
    }
  );
});